rm -rf glide64-wonder+
mkdir glide64-wonder+
cp -a . glide64-wonder+
find glide64-wonder+ -name "*.o" -exec rm {} \;
find glide64-wonder+ -name "*.ow32" -exec rm {} \;
find glide64-wonder+ -name "*.owmsvc" -exec rm {} \;
find glide64-wonder+ -name "*~" -exec rm {} \;
find glide64-wonder+ -name "*.tgz" -exec rm {} \;
find glide64-wonder+ -name "*.zip" -exec rm {} \;
find glide64-wonder+ -name "*.gz" -exec rm {} \;
find glide64-wonder+ -name "*.so" -exec rm {} \;
find glide64-wonder+ -name "*.dll" -exec rm {} \;
find glide64-wonder+ -name "*.lib" -exec rm {} \;
find glide64-wonder+ -name "*.exp" -exec rm {} \;
find glide64-wonder+ -name "*.RES" -exec rm {} \;
find glide64-wonder+ -name ".svn" -exec rm -rf {} \;
rm -rf glide64-wonder+/glide64-wonder+
tar zcf "../backups/glide64-wonder+-linux$1.tgz" glide64-wonder+
