ifneq ("$(WIN32)", "1")
GTK_FLAGS := $(shell pkg-config gtk+-2.0 --cflags) -D_GTK2
GTK_LIBS := $(shell pkg-config gtk+-2.0 --libs)
#GTK_LIBS := -lgtk-x11-2.0
endif

UNAME := $(shell uname -m)
ifeq ("$(UNAME)","x86_64")
BITS ?= 64
else
BITS ?= 32
endif

ifneq ("$(WIN32)", "1")
CC  = gcc -march=athlon64
CXX	= g++ -march=athlon64
LD	= g++ -march=athlon64
STRIP	= strip
SDL_CFLAGS := $(shell sdl-config --cflags)
SDL_LIBS := $(shell sdl-config --libs)
GLIDE_CFLAGS	= -Wall -g -DGCC -DUSE_GTK $(SDL_GLIDE_CFLAGS) $(GTK_FLAGS) -Iwrapper/ -ffast-math# -funroll-loops -fexpensive-optimizations #-fomit-frame-pointer
ifeq ("$(BITS)","64")
GLIDE_CFLAGS += -fPIC
endif
CFLAGS = -O2
#CFLAGS = -O1 -save-temps# -dA
LDFLAGS	= -g -shared -Wl,-Bsymbolic -lGL -lGLU -L/usr/X11R6/lib $(SDL_LIBS)
ifeq ("$(VPDEBUG)", "1")
GLIDE_CFLAGS	+= -DVPDEBUG
LDFLAGS += -lIL
else
LDFLAGS += -fomit-frame-pointer
endif
O	= o
COUT	= -c -o #
LOUT	= -o #
else
STRIP	= $(TOOL_PREFIX)strip
WINDRES	= $(TOOL_PREFIX)windres
ifeq ("$(MSVC)", "1")
MSVCP=/mnt/big1/msvc7.1/Vc7
MSVCP2=I:\\mnt\\big1\\msvc7.1\\Vc7
#MSVC_TOOLS_PREFIX=	(pkill -9 wine && sleep 0.1 || true) && wine --debugmsg -all -- $(MSVCP)/bin/
MSVC_TOOLS_PREFIX=	wine $(MSVCP)/bin/
#MSVC_TOOLS_PREFIX=wine --debugmsg -all -- /mnt/windaube/msvc7.1/Vc7/bin/
MSVC_CFLAGS = -I$(MSVCP2)\\include -I$(MSVCP2)\\PlatformSDK\\Include
MSVC_LDFLAGS = -LIBPATH:$(MSVCP2)\\lib -LIBPATH:$(MSVCP2)\\PlatformSDK\\Lib

CC        = $(MSVC_TOOLS_PREFIX)cl
CXX       = $(CC)
COUT      = -c -Fo
LD        = $(MSVC_TOOLS_PREFIX)link
LOUT      = -OUT:
LDPATH    = -libpath:

DEFINES += -DWIN32 -D_WINDOWS -DNT_PLUGIN -D_WIN32

GLIDE_CFLAGS	= $(DEFINES) $(MSVC_CFLAGS)
GLIDE_CFLAGS 	+= -Iwrapper
GLIDE_CFLAGS	+= -Ot -Ox

SYSLIBS += Gdi32.lib winspool.lib comdlg32.lib Opengl32.lib
SYSLIBS += advapi32.lib ole32.lib oleaut32.lib uuid.lib 
SYSLIBS += odbc32.lib odbccp32.lib comctl32.lib rpcrt4.lib
SYSLIBS += kernel32.lib user32.lib shell32.lib ws2_32.lib winmm.lib -subsystem:windows libcmt.lib

LDFLAGS = $(MSVC_LDFLAGS) -dll $(SYSLIBS)
# \
#	-export:CloseDLL \
#	-export:DllAbout \
#	-export:GetDllInfo \
#	-export:RomClosed \
#	-export:DllConfig

CPPFLAGS = $(GLIDE_CFLAGS)
O	= owmsvc
RES	= RES
else
CC	= $(TOOL_PREFIX)gcc
CXX	= $(TOOL_PREFIX)g++
LD	= $(TOOL_PREFIX)g++
GLIDE_CFLAGS	= -mno-cygwin -DGCC -Iwrapper/ -O2 -ffast-math -funroll-loops -fomit-frame-pointer
LDFLAGS	= -mno-cygwin -shared -lole32 -luuid -lcomctl32 -lwinspool -lws2_32 -lwsock32 -lopengl32 -lglu32 -lglut32 -mno-cygwin -mwindows -mconsole
O	= ow32
RES	= ow32
COUT	= -c -o #
LOUT	= -o #
endif
endif
#GLIDE_CFLAGS	= -DUSE_GTK `sdl-config --cflags` $(GTK_FLAGS) -Iwrapper/ -O2 -g
CPPFLAGS= $(GLIDE_CFLAGS)

OBJECTS = \
	Main.$(O) \
	rdp.$(O) \
	Ini.$(O) \
	TexCache.$(O) \
	Debugger.$(O) \
	Util.$(O) \
	CRC.$(O) \
	Combine.$(O) \
	TexBuffer.$(O) \
	Config.$(O) \
	3dmath.$(O) \
	DepthBufferRender.$(O)

WRAPPEROBJECTS = \
	wrapper/combiner.$(O) \
	wrapper/textures.$(O) \
	wrapper/main.$(O) \
	wrapper/geometry.$(O) \
	wrapper/config.$(O) \
	wrapper/filter.$(O) \
	wrapper/2xsai.$(O) \
	wrapper/hq2x.$(O) \
	wrapper/hq4x.$(O)

GTKOBJECTS = \
	support.$(O) \
	messagebox.$(O)

W32OBJECTS = \
	Resource.$(RES) \
	ToolTips.$(O)

W32WRAPPEROBJECTS = \
	wrapper/Script1.$(RES) \
	wrapper/window.$(O)

ifneq ("$(WIN32)", "1")
OBJECTS	+= $(GTKOBJECTS) $(WRAPPEROBJECTS)
TARGET	= Glide64.so
all: $(TARGET) instruction

GLIDE_CFLAGS += -MMD -MP -MQ $@
DEPS := $(OBJECTS:.$(O)=.d) compiletex.d
ifneq ($(MAKECMDGOALS),clean)
-include $(DEPS)
endif
else
WRAPPEROBJECTS += $(W32WRAPPEROBJECTS)
OBJECTS	+= $(W32OBJECTS)
TARGET = Glide64.dll
all: compileglide $(TARGET) instruction
ifeq ("$(GLIDE)", "1")
GLIDE_CFLAGS	+= -DGLIDEXPORT
#LDFLAGS+= -Wl,--image-base=0x18000000
else
LDFLAGS	+= glide3x.dll # -Wl,--image-base=0x10000000
endif
endif

$(TARGET): $(OBJECTS)
	$(LD) $(GTK_LIBS) $(LOUT)$@ $(OBJECTS) $(LDFLAGS)
#	$(LD) -shared $(GTK_LIBS) $(LOUT)$@ $(OBJECTS) $(LDFLAGS)
#	strip --strip-all $@
ifeq ("$(WIN32)", "1")
ifneq ("$(MSVC)", "1")
# don't know why a .comment section is being added, but it prevents the dll
# from being loaded
	$(STRIP) -R .comment $@
endif
endif

compileglide:
	make WIN32=0 compiletex
	make GLIDE=1 glide3x.dll

glide3x.dll: $(WRAPPEROBJECTS)
	$(LD) $(GTK_LIBS) $(LOUT)$@ $(WRAPPEROBJECTS) $(LDFLAGS)
#	$(LD) -shared $(GTK_LIBS) $(LOUT)$@ $(OBJECTS) $(LDFLAGS)
#	strip --strip-all $@
ifneq ("$(MSVC)", "1")
	$(STRIP) -R .comment $@
endif

Main.$(O): font.h cursor.h
font.h:	compiletex
	./compiletex font.tex font.h font

cursor.h: compiletex
	./compiletex cursor.tex cursor.h cursor

compiletex: compiletex.o
	$(LD) -o $@ compiletex.o

instruction: $(TARGET)
	@echo please copy $(TARGET) AND Glide64.ini in the plugins/ folder of the emulator

clean:
	rm -rf $(OBJECTS) $(ALL) $(DEPS) compiletex compiletex.$(O) font.h cursor.h
	rm -rf *.ii *.i *.s

rebuild:
	$(MAKE) clean
	$(MAKE) all

.SUFFIXES: .rc
.rc.$(O):
	$(WINDRES) $< -o $@

.SUFFIXES: .$(O) .cpp .c
.cpp.$(O):
# in case this was invoked because a header changed and the compilation fails,
# the record of dependency in the .d is gone, but not the .o
	@rm -f $@
	$(CXX) $(CFLAGS) $(CPPFLAGS) $< $(COUT)$@

.c.$(O):
	@rm -f $@
	$(CC) $(CFLAGS) $(GLIDE_CFLAGS) $< $(COUT)$@


.SUFFIXES: .$(RES)
.rc.RES:
	$(MSVC_TOOLS_PREFIX)rc -I$(MSVCP2)/atlmfc/include -I$(MSVCP2)/PlatformSDK/Include/ $<

.PHONY: all instruction clean rebuild
