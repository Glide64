//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Script1.rc
//
#define IDD_DIALOG1                     101
#define IDD_CONFIG_DIAL                 101
#define IDC_COMBO1                      1000
#define IDC_RES_COMBO                   1000
#define IDC_COMBO2                      1001
#define IDC_FILTER_COMBO                1001
#define IDC_GLSL                        1002
#define IDC_DITHALPHA                   1003
#define IDC_FBO                         1004
#define IDC_AUXBUF                      1005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
