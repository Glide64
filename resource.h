//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Resource.rc
//
#define IDD_CONFIG                      101
#define IDR_FONT                        116
#define IDR_CURSOR                      117
#define IDD_DEBUG                       120
#define IDD_STATISTICS                  121
#define IDB_LOGO                        127
#define IDB_ARGENTINA                   129
#define IDB_BRAZIL                      130
#define IDB_CANADA                      131
#define IDB_RUSSIA                      132
#define IDB_US                          133
#define IDB_FRANCE                      139
#define IDC_RESOLUTION                  1001
#define IDC_CHECK1                      1002
#define IDC_WIREFRAME                   1002
#define IDC_UCODE                       1003
#define IDC_FPS                         1004
#define IDC_LOGGING                     1005
#define IDC_LOGCLEAR                    1006
#define IDC_RUNINWINDOW                 1007
#define IDC_FOG                         1008
#define IDC_FPS1                        1009
#define IDC_AUTODETECT                  1010
#define IDC_FILTER                      1011
#define IDC_CACHEFILTER                 1012
#define IDC_GID1                        1013
#define IDC_GID2                        1014
#define IDC_GID3                        1015
#define IDC_GID4                        1016
#define IDC_UNKRED                      1017
#define IDC_LOGUNK                      1018
#define IDC_UNKCLEAR                    1019
#define IDC_BUFFERCLEAR                 1020
#define IDC_UCODE_T                     1021
#define IDC_RESOLUTION_T                1022
#define IDC_FILTER_T                    1023
#define IDC_WFMODE                      1024
#define IDC_FPS2                        1025
#define IDC_LOG_E                       1026
#define IDC_FPS3                        1027
#define IDC_CPU_HACK                    1027
#define IDC_CLEAR8                      1028
#define IDC_SWAP                        1028
#define IDC_DEPTH                       1029
#define IDC_LOD                         1029
#define IDC_SWAP_T                      1030
#define IDC_ALT_TEX_SIZE                1031
#define IDC_WRAP_BIG_TEX                1031
#define IDC_USE_STS1_ONLY               1032
#define IDC_FILL_DEPTH                  1032
#define IDC_LOD_T                       1032
#define IDC_CLOCK                       1033
#define IDC_NEWSWAP                     1034
#define IDC_FB_PRIMARY                  1034
#define IDC_FB_READ_ALWAYS              1034
#define IDC_CLOCK24                     1035
#define IDC_FB_INFO                     1035
#define IDC_VSYNC                       1036
#define IDC_DEPTH_T                     1037
#define IDC_FB_SMART                    1038
#define IDC_FBO                         1039
#define IDC_CRC                         1040
#define IDC_ENABLEMB                    1041
#define IDC_HIRESMB                     1043
#define IDC_HIRESFB                     1043
#define IDC_CORONA                      1044
#define IDC_DB_CLEAR                    1045
#define IDC_FB_ALPHA                    1046
#define IDC_FBO2                        1047
#define IDC_CUSTOM                      1048
#define IDC_OTHER                       1049
#define IDC_HOTKEYS                     1050
#define IDC_DB_RENDER                   1051
#define IDC_RE2_VIDEO                   1052
#define IDC_HOTKEYS2                    1053
#define IDC_WRAPPER                     1054

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        146
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1055
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
